## TĀSKS 1

Vienkārša faila augšupielāde/ lejupielāde | php

## TĀSKS 2 

Autorizācijas modulis.

- Reģistrēšanās -> saglabā lietotāja epastu/paroli db
    - Epasta lauka pārbaude vai tas atbilst standartam (var izmantot regex pārbaudei)
    - Parolei jābūt > 8 simboli 
    - Pēc veiksmīgas reģistrācijas aizmet uz login lapu.
- Autorizēšanās -> Atsevišķā lapā ļauj lietotājam ielogoties
    - Pārbauda vai lietotājs ir reģistrēts. Ja nav, piedāvā reģistrēt. 

## TĀSKS 3

- Pēc ielogošanās lietotājam parāda galveno lapu. Pagaidām tur jāizvada tikai viņa lietotājvārds.
- Lapai jāuztaisa pārbaude vai lietotājs ir ielogojies. (Ja neesi ielogojies un atver linku ar galveno lapu -> redirekts uz login lapu)


## TĀSKS 4  

Jāievieš lietotāju lomas. 

- Parastais ūseris -> visi kuri reģistrējas paši 
- Admins -> šo var piereģisrēt tikai superAdmins 
- Super Admins -> vienu šādu lietotāju ieliekat db. šim nav reģistrācijas formas


## TĀSKS 5 

Galvenā lapa 

Parastajam lietotājam pagaidām jāparāda tikai viņa pievienotos attēlus ar aprakstu.
Adminiem visu lietotāju. 

Noformējums : 

Attēlus ar aprakstu parādam card layoutā. Izmantojam bootstrapu https://getbootstrap.com/docs/4.0/components/card/
Augšā uzliek bootstrapa navbaru ar logout pogu un linku uz formu kur pievienot jaunu postu. (Attēlu)

## Tāsks 6 

Jāpievieno postiem delete/edit opcija. 
Rediģēt jāvar gan attēlu, gan tekstu. 

## Tāsks 7 

Augšā uz navbara jauna sadaļa -> find blogs 

Sadaļā redzami citi reģistrēti lietotāji (ne admini, superAdmini). Pagaidām pretī poga follow. 

## TĀSKS 8 

 - Uz navbara jauna sadaļa -> Following (Sadaļā arādam visus lietotājus, kuriem tu seko. )
 - Jauna sadaļa -> Followers (Šeit parādam visus kuri seko tev)

## TĀSKS 9 

Galvenajā lapā jāparāda gan savi posti, gan to lietotāju, kuriem tu seko 

## TĀSKS 10 

Adminiem uz navbara sadaļa - All users

Katram lietotājam pretī poga -> block. Ja admins ir nobloķējis, ielogoties nevar. (accounts netiek dzēsts)

Ja lietotājs jau ir bloķēts vai tiek nobloķēts, jāparāda -> unblock. Lietotājas atkal var ielogoties. 

PAPILDUS : 

- Starp ūseriem, kurus var bloķēt nerādīt adminus. Tikai SuperAdminam atsevišķi parādīt parastos adminus ar iespēju tos bloķēt. 

## TĀSKS 11 

- Lietotājiem iespēja postus "nolaikot" (un ja nolaikots -> unlaikot). Blakus parādās cipars ar to, cik daudzi lietotāji nospieduši like. 

- Jauna sadaļa - Liked. Tur parādam postus, kurus konkrētais lietotājs ir nolaikojis. 


