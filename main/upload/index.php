<?php

$db = mysqli_connect("localhost", "root", "", "imagedb");

// Initialize message variable

$msg = "";


// If upload button is clicked ...
if (isset($_POST['upload'])) 
{
    // Get image name
    $image = $_FILES['image']['name'];
    // Get text
    $image_text = mysqli_real_escape_string($db, $_POST['image_text']);
    $image_text = strip_tags($image_text);

    // image file directory
    $target = "images/".basename($image);

    // Connect to the database
    $db = mysqli_connect("localhost","root","","imagedb");
    // Get all existing image IDs
    $result = mysqli_query($db, "SELECT id FROM images");
    // Turn result from object -> array
    $result = (array) $result;

    // Assign ID range
    while( in_array( ($id = rand(1,1000000) ), $result ) );

    // Initialize the session
    session_start();

    //Sets the uploader as current user in session.
    $uploader = $_SESSION['username'];

    if($image == '' || $image == null || $image_text == '')
    {
        header ("Location: form.php");
    }
    else
    {
        $sql = "INSERT INTO images (id, image, image_text, uploader) VALUES ('$id', '$image', '$image_text', '$uploader')";
        mysqli_query($db, $sql);

        if (move_uploaded_file($_FILES['image']['tmp_name'], $target)) {
            $msg = "Image uploaded successfully";
        }else{
            $msg = "Failed to upload image";
        }
    
        header ("Location: main.php");
    }
}
$result = mysqli_query($db, "SELECT * FROM images");

if (isset($_POST['Delete'])) 
{
    // ImageID = ID of the post
    $imageid = $_POST['Delete'];

    // Connect to the database
    $db = mysqli_connect("localhost","root","","imagedb");

    // Delete the post from the database
    $result = mysqli_query($db, "DELETE FROM images WHERE id = '$imageid'");
    $result_likes = mysqli_query($db, "DELETE FROM likes WHERE liked_post = $imageid");
    header ("Location: main.php");
}


// If edit button is clicked

if (isset($_POST['Edit']))
{
    ID();
}

function ID()
{
    session_start();
    $_SESSION['ImgEdit'] = unserialize(base64_decode($_POST['Edit']));

    header ("Location: edit.php");
}


// If SAVE button is clicked

if (isset($_POST['FinishEditing']))
{
    session_start();
    // Connect to the database
    $db = mysqli_connect("localhost","root","","imagedb");

    // Get text
    $image_text = mysqli_real_escape_string($db, $_POST['image_text']);
    $image_text = strip_tags($image_text);

    //$result = mysqli_query($db, "REPLACE INTO images(id, image_text, image) VALUES(".$_SESSION['ImgEdit']['id'].",'$image_text','$image');");

    $image = $_FILES['image']['name'];
    if($image == null)
    {
        $result = mysqli_query($db, "UPDATE images SET image_text = '$image_text' WHERE id = ".$_SESSION['ImgEdit']['id']."");
    }
    else
    {
        $result = mysqli_query($db, "UPDATE images SET image_text = '$image_text', image = '$image' WHERE id = ".$_SESSION['ImgEdit']['id']."");

        // image file directory
        $target = "images/".basename($image);

        if (move_uploaded_file($_FILES['image']['tmp_name'], $target)) 
        {
            $msg = "Image uploaded successfully";
        }
        else
        {
            $msg = "Failed to upload image";
        }
    }   

    header ("Location: main.php");
} 

// If FOLLOW button is clicked

if(isset($_POST['Follow']))
{
    session_start();

    // Connect to the database
    $db = mysqli_connect("localhost","root","","logindb");

    $check = mysqli_query($db, "SELECT * FROM followers WHERE follower_id = ".$_SESSION['id']." AND followed_id = ".$_POST['Follow']."");
    

    if(mysqli_num_rows($check) == null)
    {
        $result = mysqli_query($db, "INSERT INTO followers (follower_id, followed_id) VALUES (".$_SESSION['id'].",".$_POST['Follow'].")");
    }

    header ("Location: main.php");
}


if(isset($_POST['Unfollow']))
{
    session_start();

    // Connect to the database
    $db = mysqli_connect("localhost","root","","logindb");

    $check = mysqli_query($db, "SELECT * FROM followers WHERE follower_id = ".$_SESSION['id']." AND followed_id = ".$_POST['Unfollow']."");
    
    if(mysqli_num_rows($check) == 1)
    {
        $result = mysqli_query($db, "DELETE FROM followers WHERE follower_id = ".$_SESSION['id']." AND followed_id = ".$_POST['Unfollow']."");
    }

    header ("Location: main.php");
}

if(isset($_POST['Remove']))
{
    session_start();

    // Connect to the database
    $db = mysqli_connect("localhost","root","","logindb");

    $check = mysqli_query($db, "SELECT * FROM followers WHERE follower_id = ".$_POST['Remove']." AND followed_id = ".$_SESSION['id']."");
    
    if(mysqli_num_rows($check) == 1)
    {
        $result = mysqli_query($db, "DELETE FROM followers WHERE follower_id = ".$_POST['Remove']." AND followed_id =  ".$_SESSION['id']."");
    }

    header ("Location: main.php");
}

if(isset($_POST['Block']))
{
    session_start();

    // Connect to the database
    $db = mysqli_connect("localhost","root","","logindb");

    $block = mysqli_query($db, "UPDATE users SET isblocked = 1 WHERE id = ".$_POST['Block']."");

    header ("Location: main.php");
}

if(isset($_POST['Unblock']))
{
    session_start();

    // Connect to the database
    $db = mysqli_connect("localhost","root","","logindb");

    $block = mysqli_query($db, "UPDATE users SET isblocked = 0 WHERE id = ".$_POST['Unblock']."");

    header ("Location: main.php");
}

if(isset($_POST['Like']))
{
    session_start();

    // Connect to the database
    $db = mysqli_connect("localhost","root","","imagedb");

    $check = mysqli_query($db, "SELECT * FROM likes WHERE like_id = ".$_SESSION['id']." AND liked_post = ".$_POST['Like']."");

    $_SESSION['likeID'] = $_POST['Like'];
    //echo $_SESSION['likeID'];

    if(mysqli_num_rows($check) == null)
    {
        $result = mysqli_query($db, "INSERT INTO likes (like_id, liked_post) VALUES (".$_SESSION['id'].",".$_POST['Like'].")");  
    }
    else
    {
        $result = mysqli_query($db, "DELETE FROM likes WHERE like_id = ".$_SESSION['id']." AND liked_post = ".$_POST['Like']."");
    }

    $like_query = mysqli_query($db, "SELECT * FROM likes WHERE liked_post = ".$_POST['Like']."");
    $likes = mysqli_num_rows($like_query);
    $update_like_count = mysqli_query($db, "UPDATE images SET likes = ".$likes." WHERE id = ".$_POST['Like']." ");

    header ("Location: main.php");
}
?>