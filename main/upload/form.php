<?php
    include 'index.php';
    // Initialize the session
    session_start();
    
    // Check if the user is already logged in and check his role
    if($_SESSION["loggedin"] === true)
    { }
    else
    {
        header("location: ../login/login.php");
        exit;
    }
?>

<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="index.css">
    </head>
    <body id="formBody">
        <div id="mainbox2">
            <form id="formUpload" method="POST" action="index.php" enctype="multipart/form-data">

                <input type="hidden" name="size" value="1000000">

                <div id="textareaDiv">
                    <textarea maxlength="50" id="text" cols="25" rows="2" name="image_text" placeholder="Say something about this image..."></textarea>
                </div>

                <div id="uploadBdiv">
                    <div id="labelDiv">
                        <label class="btn btn-primary uploadBform" for="uploadB">
                            <p id="uploadBt">Click here to Upload an Image</p>
                            <input id="uploadB" type="file" name="image">
                        </label>   
                    </div>  
                    <button class="btn btn-success submitBform" type="submit" name="upload" onclick="reload()">Submit your Image</button>
                </div>

            </form>
        </div>
    </body>
</html>