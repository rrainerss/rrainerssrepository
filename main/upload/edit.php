<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="index.css">

    </head>
    <body id="body3">
        <div id="mainbox3">
            <form id="mainform3" method="POST" action="index.php" enctype="multipart/form-data">
                <input type="hidden" name="size" value="1000000">
                <div id="phpDiv3">
                    <?php
                        session_start();

                        if($_SESSION["loggedin"] === true)
                        { }
                        else
                        {
                            header("location: ../login/login.php");
                            exit;
                        }

                        echo    "<img src='images/".$_SESSION["ImgEdit"]["image"]."' id='img3' class='card-img-top' alt=''>";
                    ?>
                    <textarea id="textarea3" maxlength="50" id="" cols="45" rows="2" name="image_text" placeholder="Enter a description about the image...">
                        <?php
                        echo $_SESSION["ImgEdit"]["image_text"];
                        ?>
                    </textarea>
                </div>

                <div>
                    <label id="uploadButtonForm" class="btn btn-primary" for="uploadBform">
                        <p id="">Change Image</p>
                        <input id="uploadBform" type="file" name="image">
                    </label>   
                </div><br> 
                <form id="" method="POST" action="index.php" enctype="multipart/form-data">
                    <button class="btn btn-success submitEdit3" type="submit" name="FinishEditing" onclick="reload()">Save</button>
                </form>
            </form>
        </div>
    </body>
</html>