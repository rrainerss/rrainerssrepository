<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="index.css">
    </head>
    <body>


    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="#">Image Database</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a href="../upload/form.php" class="btn btn-success signout">Upload Your Images</a>
                    </li>
                    
                    <li class="nav-item dropdown">
                        <a data-toggle="dropdown" class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">All Users
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <?php
                                    session_start();

                                    $db = mysqli_connect("localhost","root","","logindb");
                                    $result = mysqli_query($db, "SELECT * FROM users");

                                    while($row = mysqli_fetch_array( $result )) 
                                    {
                                        if($_SESSION['isadmin'] == 0)
                                        {
                                            if($_SESSION["id"] == $row["id"] || $row["isadmin"] == 1)
                                            {
                                            }
                                            else if($row['isadmin'] == 0)
                                            {
                                                echo   '<a class="custom-item dropdown-item" >';
                                                echo        $row["username"];
                                                echo        '<div class="dropdownFormDiv"><form method="POST" action="index.php">';
                                                echo            '<button href="" class="btn btn-success followBtn" name="Follow" value='.$row["id"].'>Follow</button>';
                                                echo        '<form></div>';
                                                echo    '</a>';
                                            }
                                        }
                                        else
                                        {
                                            if($row["id"] == $_SESSION["id"])
                                            {}
                                            else if($_SESSION["issuperadmin"] == 1 && $row["isblocked"] == 0)
                                            {
                                                echo   '<a class="custom-item dropdown-item" >';
                                                echo        $row["username"];
                                                echo        '<div class="dropdownFormDiv"><form method="POST" action="index.php">';
                                                echo            AdminBlockUnblock($row,"Block");
                                                echo        '<form></div>';
                                                echo    '</a>';
                                            }
                                            else if($row["isadmin"] == 1 && $row["isblocked"] == 0)
                                            {}
                                            else if($row["isblocked"] == 0)
                                            {
                                                echo   '<a class="custom-item dropdown-item" >';
                                                echo        $row["username"];
                                                echo        '<div class="dropdownFormDiv"><form method="POST" action="index.php">';
                                                echo            AdminBlockUnblock($row,"Block");
                                                echo        '<form></div>';
                                                echo    '</a>';
                                            }
                                            else
                                            {
                                                echo   '<a class="custom-item dropdown-item" >';
                                                echo        $row["username"];
                                                echo        '<div class="dropdownFormDiv"><form method="POST" action="index.php">';
                                                echo            AdminBlockUnblock($row,"Unblock");
                                                echo        '<form></div>';
                                                echo    '</a>';
                                            }
                                        }

                                    } 

                                ?>
                        </div>
                    </li>            
                    
                    <li class="nav-item dropdown">
                        <a data-toggle="dropdown" class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Following
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <?php
                                //error_reporting(0);
                                //ini_set('display_errors', 0);

                                $db = mysqli_connect("localhost","root","","logindb");

                                $follow = mysqli_query($db, "SELECT followed_id FROM followers WHERE follower_id = ".$_SESSION['id'].""); 

                                $follow_query = mysqli_query($db, "SELECT followed_id FROM followers WHERE follower_id = ".$_SESSION['id']."");


                                if(mysqli_num_rows($follow_query) !== 0)
                                {
                                    while($followed_user_id_fetch = mysqli_fetch_array($follow_query))
                                    {
                                            
                                        $followed_user_id_fetch = array_unique($followed_user_id_fetch);
                                        foreach($followed_user_id_fetch as $followed_user_id)
                                        {
                                            $followed_user_query = mysqli_query($db, "SELECT username FROM users WHERE id = $followed_user_id");
                                            $followed_user_fetch = mysqli_fetch_array($followed_user_query);
                                            $followed_user_name = array_unique($followed_user_fetch);
                                            $followed_user_name = implode($followed_user_name);

                                            echo   '<a class="custom-item dropdown-item" >';
                                            echo        $followed_user_name;
                                            echo        '<div class="dropdownFormDiv"><form method="POST" action="index.php">';
                                            echo            '<button href="" class="btn btn-danger followBtn2" name="Unfollow" value='.$followed_user_id.'>Unfollow</button>';
                                            echo        '<form></div>';
                                            echo    '</a>';
                                        }
                                    }  
                                }
                                else
                                {
                                    echo    '<a class="custom-item dropdown-item" >';
                                    echo    "You aren't following anyone yet!";
                                    echo    '</a>';   
                                }
                            ?>
                        </div>
                    </li> 

                    <li class="nav-item dropdown">
                        <a data-toggle="dropdown" class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Followers
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <?php

                                $db = mysqli_connect("localhost","root","","logindb");

                                $follow = mysqli_query($db, "SELECT follower_id FROM followers WHERE followed_id = ".$_SESSION['id']."");  //.$_SESSION['id'].

                                $follow_query = mysqli_query($db, "SELECT follower_id FROM followers WHERE followed_id = ".$_SESSION['id']."");

                                if(mysqli_num_rows($follow_query) !== 0)
                                {
                                    while($followed_user_id_fetch = mysqli_fetch_array($follow_query))
                                    {
                                            
                                        $followed_user_id_fetch = array_unique($followed_user_id_fetch);
                                        foreach($followed_user_id_fetch as $followed_user_id)
                                        {
                                            $followed_user_query = mysqli_query($db, "SELECT username FROM users WHERE id = $followed_user_id");
                                            $followed_user_fetch = mysqli_fetch_array($followed_user_query);
                                            $followed_user_name = array_unique($followed_user_fetch);
                                            $followed_user_name = implode($followed_user_name);

                                            echo   '<a class="custom-item dropdown-item" >';
                                            echo        $followed_user_name;
                                            echo        '<div class="dropdownFormDiv"><form method="POST" action="index.php">';
                                            echo            '<button href="" class="btn btn-danger followBtn2" name="Remove" value='.$followed_user_id.'>Remove</button>';
                                            echo        '<form></div>';
                                            echo    '</a>';
                                        }
                                    }  
                                }
                                else
                                {
                                    echo    '<a class="custom-item dropdown-item" >';
                                    echo    "No one's following you yet!";
                                    echo    '</a>';   
                                }
                            ?>
                        </div>
                    </li> 

                </ul>
                <div style="color:white;" class="btn"><?php echo $_SESSION["username"] ?></div>
                <a href="../login/logout.php" class="btn btn-danger signout">Sign out</a>
                <a href="../login/welcome.php" class="btn btn-warning signout">Back</a>
            </div>
        </nav>


        <div id="mainbox">
            <p id="AllPostsText">All Posts</p>
            <?php
                // Check if the user is already logged in and check his role
                if($_SESSION["loggedin"] === true)
                { }
                else
                {
                    header("location: ../login/login.php");
                    exit;
                }

                include 'index.php';


                if($_SESSION["isadmin"] == 0)
                {
                    while ($row = mysqli_fetch_array($result)) 
                    {
                        if($row["uploader"] == $_SESSION["username"])
                        {
                            $passrow = base64_encode(serialize($row));
                            echo    "<div class='card' id=".$row['id']." style='width: 18rem;'>";
                            echo       "<img src='images/".$row['image']."' class='card-img-top' alt=''>";
                            echo       "<div class='card-body'>";
                            echo           "<h5 class='card-title'>".$row['image_text']."</h5>";
                            echo           "<form style='padding-bottom:0; padding-left:0;'  id='form' method='POST' action='index.php' enctype='multipart/form-data'>";
                            echo                "<div style='display:table;'>";
                            AllButtons(Likes($row),DeleteAndEdit($row,$passrow));
                            echo                "</div>";
                            echo           "</form>";
                            echo       "</div>";
                            echo    "</div>";
                        }
                    }

                    // Other Posts

                    // Connecting to post/image database
                    $idb = mysqli_connect("localhost","root","","imagedb");
                    // Connect to login and follow databases
                    $db = mysqli_connect("localhost","root","","logindb");

                    $follow_query = mysqli_query($db, "SELECT followed_id FROM followers WHERE follower_id = ".$_SESSION['id']."");

                    while($followed_user_id_fetch = mysqli_fetch_array($follow_query))
                    {
                        $followed_user_id_fetch = array_unique($followed_user_id_fetch);
                        foreach($followed_user_id_fetch as $followed_user_id)
                        {
                            $followed_user_query = mysqli_query($db, "SELECT username FROM users WHERE id = $followed_user_id");
                            $followed_user_fetch = mysqli_fetch_array($followed_user_query);
                            $followed_user_name = array_unique($followed_user_fetch);
                            $followed_user_name = implode($followed_user_name);

                            $result = mysqli_query($idb, "SELECT * FROM images");
                            
                            while ($row = mysqli_fetch_array($result))
                            {
                                if($row["uploader"] == $followed_user_name)
                                {
                                    echo    "<div class='card' id=".$row['id']." style='width: 18rem;'>";
                                    echo       "<img src='images/".$row['image']."' class='card-img-top' alt=''>";
                                    echo       "<div class='card-body'>";
                                    echo           "<h5 class='card-title'>".$row['image_text']."</h5>";
                                    echo           "<div>By $followed_user_name</div>";
                                    echo           "<form id='form' method='POST' action='index.php' enctype='multipart/form-data'>";
                                    AllButtons(Likes($row));
                                    echo           "</form>";
                                    echo       "</div>";
                                    echo    "</div>";
                                }
                            }
                        }
                    }
                } 
                else
                {
                    while ($row = mysqli_fetch_array($result)) 
                    {
                        $passrow = base64_encode(serialize($row));
                        echo    "<div class='card' id=".$row['id']." style='width: 18rem;'>";
                        echo       "<img src='images/".$row['image']."' class='card-img-top' alt=''>";
                        echo       "<div class='card-body'>";
                        echo           "<h5 class='card-title'>".$row['image_text']."</h5>";
                        echo           "<div>By ".$row['uploader']."</div>";
                        echo           "<form id='form' method='POST' action='index.php' enctype='multipart/form-data'>";
                        echo                "<div style='display:table;'>";
                        AllButtons(Likes($row),DeleteAndEdit($row,$passrow));
                        echo                "</div>";
                        echo           "</form>";
                        echo       "</div>";
                        echo    "</div>";
                    }
                }
                
                // echo "<script>alert('" . json_encode($_SESSION["isadmin"]) . "');</script>";
                // Debug
            ?>
            <p id="LikedPostsText">Liked Posts</p>
            <?php
                // Connecting to post/image database
                $idb = mysqli_connect("localhost","root","","imagedb");
                $liked_posts = mysqli_query($idb, "SELECT * FROM likes WHERE like_id = ".$_SESSION['id']."");
                if(mysqli_num_rows($liked_posts) > 0)
                {
                    while ($post = mysqli_fetch_array($liked_posts)) 
                    {
                        $row = mysqli_query($idb, "SELECT * FROM images WHERE id=".$post['liked_post']."");
                        $row = mysqli_fetch_array($row);
                        echo    "<div class='card' id=".$row['id']." style='width: 18rem;'>";
                        echo       "<img src='images/".$row['image']."' class='card-img-top' alt=''>";
                        echo       "<div class='card-body'>";
                        echo           "<h5 class='card-title'>".$row['image_text']."</h5>";
                        echo           "<form id='form' method='POST' action='index.php' enctype='multipart/form-data'>";
                        echo                "<div>";
                        AllButtons(Likes($row));
                        echo                "</div>";
                        echo           "</form>";
                        echo       "</div>";
                        echo    "</div>";
                    }
                }
                else
                {
                    echo "<p id='noLikedPosts'>You haven't liked any posts yet!</p>";
                }


                function AdminBlockUnblock($row,$block)
                {
                    echo    '<button href="" class="btn btn-secondary followBtn2" name='.$block.' value='.$row["id"].'>'.$block.'</button>';
                }


                function Likes($row)
                {
                    $idb = mysqli_connect("localhost","root","","imagedb");
                    $query = mysqli_query($idb, "SELECT * FROM likes WHERE like_id = ".$_SESSION['id']." AND liked_post = ".$row['id']."");
                    if(mysqli_num_rows($query) > 0)
                    {
                        echo    "<div style='background-color:red;display:table-cell;'><button href='#' style='margin-right:5px; float:right; width:80px;' class='btn btn-secondary red' name='Like' value=".$row['id'].">❤️".$row['likes']."</button></div>";
                    }
                    else
                    {
                        echo    "<div style='background-color:gray;display:table-cell;'><button href='#' style='margin-right:5px; float:right; width:80px;' class='btn btn-secondary gray' name='Like' value=".$row['id'].">❤️".$row['likes']."</button></div>";
                    }
                }

                function DeleteAndEdit($row,$passrow)
                {
                    echo    "<button type='submit' href='#' class='btn btn-danger' name='Delete' value=".$row['id'].">Delete</button>";
                    echo    "<button href='#' style='margin-right:10px; float:left; width:80px;' class='btn btn-primary' name='Edit' value=".$passrow.">Edit</button>";
                }

                function AllButtons($likes,$buttons = null)
                {
                    $likes;
                    if($buttons == null)
                    { }
                    else
                    {
                        $buttons;
                    }
                }
            ?>

        </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    </body>
</html>
