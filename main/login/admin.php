<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="../upload/index.css" type="text/css">
        <script>
        function refresh()
        {
            location.reload(forceGet);
        }
        </script>
    </head>
    <body id="adminBody">
        <div id="spawnBox">
            <?php
                session_start(); // start session

                // Check if user is a SA
                if (($_SESSION["issuperadmin"] != 1)) {
                    header("location: nopermission.php");
                    exit;
                }

                $db = mysqli_connect("localhost","root","","logindb");
                $result = mysqli_query($db, "SELECT * FROM users");

                while ($row = mysqli_fetch_array($result))
                {
                    $id = $row['id'];
                echo "<div class='card' id='user'>";
                    echo "<p>".$row['username']."</p>";
                    if($row['username'] == $_SESSION['username'])
                    {
                    }
                    else if($row['isadmin'] == 0)
                    {
                        $button_name = "Admin";
                        echo    '<form class="form1" method="post" action="index.php">
                                    <input id="SaveButton" class="btn btn-warning" type="submit" name='.$button_name.' class="button" value='.$button_name.' onclick="refresh()">
                                    <input id="SaveButton" class="btn btn-warning" type="hidden" name="AdminVal" class="button" value='.$row['username'].'>
                                </form>';
                    }
                    else
                    {
                        $button_name = "User";
                        echo    '<form class="form1" method="post" action="index.php">
                                    <input id="SaveButton" class="btn btn-success" type="submit" name='.$button_name.' class="button" value='.$button_name.' onclick="refresh()">
                                    <input id="SaveButton" class="btn btn-success" type="hidden" name="UserVal" class="button" value='.$row['username'].'>
                                </form>';
                    }
                echo '</div>';
                }
            ?>

        </div>     

        <a href="../login/logout.php" class="SignOutBtn btn btn-danger">Sign Out of Your Account</a>
        <a href="../login/welcome.php" class="SignOutBtn btn btn-warning">Back</a>
    </body>
</html>
