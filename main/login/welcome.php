<?php
// Initialize the session
session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

if($_SESSION["issuperadmin"] == 1)
{
    $RoleMessage = "You're a Super Admin";
}
else if($_SESSION["isadmin"] == 1)
{
    $RoleMessage = "You're an Admin";
}
else
{
    $RoleMessage = "You're a user";
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body { font: 14px sans-serif; text-align: center; }
    </style>
</head>
<body>
    <div class="page-header">
        <h1>Hi,<b>
                <?php
                    $splitname = substr($_SESSION["username"], 0, strpos($_SESSION["username"], '@'));
                    $splitname .= ".";
                    echo htmlspecialchars(rtrim($splitname));
                ?>
            </b> Welcome!</h1>
        <h1><b><?php echo htmlspecialchars($RoleMessage); ?></b>!</h1>
    </div>
    <p>
        <a href="../upload/main.php" class="btn btn-success">Image Upload</a>
        <a href="logout.php" class="btn btn-danger">Sign Out</a>
        <?php
        if($_SESSION["issuperadmin"] == 1)
        {
            echo '<a href="admin.php" class="btn btn-primary">Admin Page</a>';
        } 
        ?>
    </p>
</body>
</html>